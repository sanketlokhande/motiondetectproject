#pragma once
#ifndef __DLL_INTERFACE_H__
#define __DLL_INTERFACE_H__
#include "BYTES.h"
#include "project_motionDetect.h"
typedef void* MOTION_HANDLE;

extern "C" __declspec(dllexport) MOTION_HANDLE motion_init(unsigned int _u4Sample_size) noexcept;
extern "C" __declspec(dllexport) void motion_free(MOTION_HANDLE& __handle) noexcept;

extern "C" __declspec(dllexport) void motion_first_run(const MOTION_HANDLE __handle, project_motionDetect::project_motionData* __buffer) noexcept;
extern "C" __declspec(dllexport) BYTE   motion_process(const MOTION_HANDLE __handle, project_motionDetect::project_motionData* __buffer) noexcept;
#endif //__DLL_INTERFACE_H__