//#define _CRTDBG_MAP_ALLOC
//#include <stdlib.h>
//#include <crtdbg.h>
//#define new	new(_CLIENT_BLOCK , __FILE__ , __LINE__)


#include "pch.h"
#include <iostream>
#include "project_motionDetect.h"
#include "project_circBuffer.h"
#include "project_circBuffer.cpp"
#include <fstream>
#include <vector>
#include <iomanip>
#include "BYTES.h"

using namespace std;
using namespace chrono;

constexpr size_t BUFFER_SIZE = (1 << 10);
constexpr size_t STEP_SIZE = (1 << 3);

int main(void)
{
	size_t ax_c(NULL),
		ay_c(NULL),
		az_c(NULL),
		gx_c(NULL),
		gy_c(NULL),
		gz_c(NULL),
		sub_count(NULL),
		five_sec_count(NULL);
	double hold_value(static_cast<double>(NULL));
	double* ax(new double[BUFFER_SIZE]),
		* ay(new double[BUFFER_SIZE]),
		* az(new double[BUFFER_SIZE]),
		* gx(new double[BUFFER_SIZE]),
		* gy(new double[BUFFER_SIZE]),
		* gz(new double[BUFFER_SIZE]),
		* tempBuff(new double[BUFFER_SIZE]);

	memset(ax, NULL, sizeof(double) * BUFFER_SIZE);
	memset(ay, NULL, sizeof(double) * BUFFER_SIZE);
	memset(az, NULL, sizeof(double) * BUFFER_SIZE);
	memset(gx, NULL, sizeof(double) * BUFFER_SIZE);
	memset(gy, NULL, sizeof(double) * BUFFER_SIZE);
	memset(gz, NULL, sizeof(double) * BUFFER_SIZE);

	bool _eofSentinal(false), _ranOnce(false);
	BYTE _finalOutput;
	project_motionData data_struct(ax, ay, az, gx, gy, gz);
	project_motionDetect _motion(BUFFER_SIZE, STEP_SIZE);
	stringstream ss;
	string _charBuffer, _lineBuffer;
	ifstream data_reader("test.csv");
	ofstream data_writer("output.csv");
	time_point<steady_clock> t1, t2;

	if (!data_reader.is_open())
	{
		cout << "File not opened.";
		exit(-99);
	}

	do {
		ss.clear();
		getline(data_reader, _lineBuffer, '\n');
		ss << _lineBuffer;
		for (size_t count = 0; count < 7; count++)
		{
			getline(ss, _charBuffer, ',');
			try {
				hold_value = stod(_charBuffer);
			}
			catch (exception) {
				if (data_reader.eof())
					goto END_FILE;
			}

			switch (count)
			{
			case 1:
				if (ax_c < BUFFER_SIZE)
				{
					ax[ax_c] = hold_value;
					ax_c++;
				}
				else
				{
					memcpy(tempBuff, &ax[1], sizeof(double) * (BUFFER_SIZE - 1));
					memcpy(ax, tempBuff, sizeof(double) * BUFFER_SIZE);
					ax[ax_c - 1] = hold_value;
				}
				continue;

			case 2:
				if (ay_c < BUFFER_SIZE)
				{
					ay[ay_c] = hold_value;
					ay_c++;
				}
				else
				{
					memcpy(tempBuff, &ay[1], sizeof(double) * (BUFFER_SIZE - 1));
					memcpy(ay, tempBuff, sizeof(double) * BUFFER_SIZE);
					ay[ay_c - 1] = hold_value;
				}
				continue;

			case 3:
				if (az_c < BUFFER_SIZE)
				{
					az[az_c] = hold_value;
					az_c++;
				}
				else
				{
					memcpy(tempBuff, &az[1], sizeof(double) * (BUFFER_SIZE - 1));
					memcpy(az, tempBuff, sizeof(double) * BUFFER_SIZE);
					az[az_c - 1] = hold_value;
				}
				continue;

			case 4:
				if (gx_c < BUFFER_SIZE)
				{
					gx[gx_c] = hold_value;
					gx_c++;
				}
				else
				{
					memcpy(tempBuff, &gx[1], sizeof(double) * (BUFFER_SIZE - 1));
					memcpy(gx, tempBuff, sizeof(double) * BUFFER_SIZE);
					gx[gx_c - 1] = hold_value;
				}
				continue;

			case 5:
				if (gy_c < BUFFER_SIZE)
				{
					gy[gy_c] = hold_value;
					gy_c++;
				}
				else
				{
					memcpy(tempBuff, &gy[1], sizeof(double) * (BUFFER_SIZE - 1));
					memcpy(gy, tempBuff, sizeof(double) * BUFFER_SIZE);
					gy[gy_c - 1] = hold_value;
				}
				continue;

			case 6:
				if (gz_c < BUFFER_SIZE)
				{
					gz[gz_c] = hold_value;
					gz_c++;
				}
				else
				{
					memcpy(tempBuff, &gz[1], sizeof(double) * (BUFFER_SIZE - 1));
					memcpy(gz, tempBuff, sizeof(double) * BUFFER_SIZE);
					gz[gz_c - 1] = hold_value;
				}
				continue;
			}
		}

		if (sub_count < STEP_SIZE)
			sub_count++;
		else
		{
			sub_count = 0;
			if (_ranOnce)
			{
				t1 = high_resolution_clock::now();
				_finalOutput = _motion.process_samples(&data_struct);
				t2 = high_resolution_clock::now();
			}
			else
			{
				if (five_sec_count >= BUFFER_SIZE)
				{
					_motion.run_once_at_start(&data_struct);
					_ranOnce = true;
				}
			}
			five_sec_count += STEP_SIZE;

			data_writer << static_cast<int>(five_sec_count) << ',' << static_cast<int>(_finalOutput.BYTES) << ',' << duration_cast<microseconds>(t2 - t1).count() << endl;
			//cout << "Sample number: " << static_cast<int>(five_sec_count) << endl;
			//cout << "Motion Value: " << static_cast<int>(_finalOutput.BYTES) << endl;
			//cout << "Duration: " << duration_cast<microseconds>(t2 - t1).count() << endl;
			//this_thread::sleep_for(milliseconds(20));
			//system("cls");
		}

		_eofSentinal = data_reader.eof();
	} while (!_eofSentinal);

END_FILE:

	data_writer.close();
	data_reader.close();

	delete[] ax;
	delete[] ay;
	delete[] az;
	delete[] gz;
	delete[] gy;
	delete[] gx;
	delete[] tempBuff;
	return 69;
}

//#define SAMPLE_NUM 200
//
//project_motionDetect motion(SAMPLE_NUM);
//project_motionDetect::project_motionData *input_motionData = new project_motionDetect::project_motionData;
//project_circBuffer<double> *input_circBuffer[6];
//using namespace std;
//
//enum BUFFER_ID : UINT
//{
//	X_ACCEL,
//	Y_ACCEL,
//	Z_ACCEL,
//	X_GYRO,
//	Y_GYRO,
//	Z_GYRO,
//	NUM_BUFFERS
//};
//template <typename T>
//constexpr inline T& DREF(T* __restrict tPTR) noexcept
//{
//	return *tPTR;
//}
//inline project_circBuffer<double>& BUFFREF(BUFFER_ID _ID) 
//{
//	return DREF(input_circBuffer[_ID]);
//}
//template <BUFFER_ID _ID>
//inline double& buffAccess(const unsigned int& _u4IDX) noexcept
//{
//	static_assert(_ID < NUM_BUFFERS);
//	return DREF(input_circBuffer[_ID])[_u4IDX];
//}
//
//int main()
//{
//
//	input_circBuffer[0] = new project_circBuffer<double>(SAMPLE_NUM);
//	input_circBuffer[1] = new project_circBuffer<double>(SAMPLE_NUM);
//	input_circBuffer[2] = new project_circBuffer<double>(SAMPLE_NUM);
//	input_circBuffer[3] = new project_circBuffer<double>(SAMPLE_NUM);
//	input_circBuffer[4] = new project_circBuffer<double>(SAMPLE_NUM);
//	input_circBuffer[5] = new project_circBuffer<double>(SAMPLE_NUM);
//	input_motionData->accel_x = new double[SAMPLE_NUM];
//	input_motionData->accel_y = new double[SAMPLE_NUM];
//	input_motionData->accel_z = new double[SAMPLE_NUM];
//	input_motionData->gyro_x = new double[SAMPLE_NUM];
//	input_motionData->gyro_y = new double[SAMPLE_NUM];
//	input_motionData->gyro_z = new double[SAMPLE_NUM];
//
//	for (uint32_t i = 0; i < SAMPLE_NUM; i++)
//	{
//		input_motionData->accel_x[i] = 0.00;
//		input_motionData->accel_y[i] = 0.00;
//		input_motionData->accel_z[i] = 0.00;
//
//		input_motionData->gyro_x[i] = 0.00;
//		input_motionData->gyro_y[i] = 0.00;
//		input_motionData->gyro_z[i] = 0.00;
//	}
//	
//	std::cout << "Motion Detect Algorithm!\n";
//	std::vector<vector<double>> csv_inputs;
//	string line, field;
//	istringstream linestream(line);
//	ifstream myfile("gyro3_points.csv");
//	int a = 0, b=0;
//
//	if (myfile.is_open())
//	{
//		while (getline(myfile, line))
//		{
//			stringstream sep(line);
//			csv_inputs.push_back(vector<double>());
//			a++;
//			while (getline(sep, field, ',') && a > 1)
//			{
//				csv_inputs.back().push_back(stod(field));
//			}
//		}
//		myfile.close();
//	}
//	else { 
//		cout << "Unable to open file" << "\n"; 
//		return -1;
//	}
//
//	a = 0;
//	b = 0;
//	int idx = 0;
//
//	/*do {
//		input_circBuffer[0]->add_data(idx);
//	} while (++idx< SAMPLE_NUM);*/
//
//	idx = 0;
//
//	std::ofstream writer_file;
//	writer_file.open("CSV_writer.csv", std::ofstream::out | std::ofstream::app);
//	//BYTE out;
//
//	motion.set_mean_manually(-0.0158f, 0.9745f, -0.1922f,  -0.0097f, 0.0135f, 0.0011f);
//
//	unsigned int counter = 0;
//	for (vector<vector<double>>::iterator it = csv_inputs.begin(); it != csv_inputs.end(); ++it)
//	{
//		for (vector<double>::iterator jt = it->begin(); jt != it->end(); ++jt)
//		{
//			input_circBuffer[a]->add_data(*jt); 
//			a++;
//			if (a > 5)
//			{
//				a = 0;
//				counter++;
//				do {
//					input_motionData->accel_x[idx] = buffAccess<X_ACCEL>(idx);
//					input_motionData->accel_y[idx] = buffAccess<Y_ACCEL>(idx);
//					input_motionData->accel_z[idx] = buffAccess<Z_ACCEL>(idx);
//					input_motionData->gyro_x[idx] = buffAccess<X_GYRO>(idx);
//					input_motionData->gyro_y[idx] = buffAccess<Y_GYRO>(idx);
//					input_motionData->gyro_z[idx] = buffAccess<Z_GYRO>(idx);
//				} while (++idx < SAMPLE_NUM);
//				
//				writer_file << static_cast<int> (motion.process_samples(input_motionData).BYTES) << endl;
//				idx = 0;
//			}
//		}
//	}
//	writer_file.close(); 
//
//	delete DREF(input_circBuffer);
//	DREF(input_circBuffer) = nullptr;
//
//	delete input_motionData->accel_x;
//	input_motionData->accel_x = nullptr;
//	
//	delete input_motionData->accel_y;
//	input_motionData->accel_y = nullptr;
//
//	delete input_motionData->accel_z;
//	input_motionData->accel_z = nullptr;
//
//	delete input_motionData->gyro_x;
//	input_motionData->gyro_x = nullptr;
//
//	delete input_motionData->gyro_y;
//	input_motionData->gyro_y = nullptr;
//
//	delete input_motionData->gyro_z;
//	input_motionData->gyro_z = nullptr;
//	
//	delete input_motionData;
//	input_motionData = nullptr;
//
//	///_CrtDumpMemoryLeaks();
//
//	return 0;
//}

