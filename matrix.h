#pragma once
#ifndef __MATRIX_H__
#define __MATRIX_H__
#include <stdlib.h>
#include <stdexcept>
#include <vector>
#include <complex>
#include <limits>
#include <functional>
#include <iostream>

#if defined (_MSC_VER)
	#define FORCE_INLINE __forceinline
#elif defined (__GNUC__)
	#define FORCE_INLINE __attribute__((always_inline)) inline
#endif

constexpr float FLOAT_ZERO				= std::numeric_limits<float>::epsilon();		// 0.000000119209F                                       OLD VALUE := 0.0000000001F;
constexpr double DOUBLE_ZERO			= std::numeric_limits<double>::epsilon();		// 0.000000000000000222045								 OLD VALUE := 0.000000000000001;
constexpr long double LDOUBLE_ZERO		= std::numeric_limits<long double>::epsilon();	// 0.000000000000000222045L								 OLD VALUE := 0.00000000000000000000001L;
constexpr size_t DEFAULT_ITERATIONS		= 200U;

enum LU_DECOMP_ID : size_t
{
	L_MATRIX,
	U_MATRIX,
	P_MATRIX,
	NUM_OF_LUMATRIX
};

enum QR_DECOMP_ID : size_t
{
	Q_MATRIX,
	R_MATRIX,
	NUM_OF_QRMATRIX
};

#if defined (FLOATING_PRECISION)
	#define EPSILON_NULL FLOAT_ZERO
#elif defined (DOUBLE_PRECISION)
	#define EPSILON_NULL DOUBLE_ZERO
#elif defined (LONG_DOUBLE_PRECISION)
	#define EPSILON_NULL LDOUBLE_ZERO
#elif defined (CUSTOM_PRECISION)
	#define EPSILON_NULL CUSTOM_PRECISION
#else
	#define EPSILON_NULL FLOAT_ZERO
#endif

#define TYPE_ERROR_MSG				"Error illegal type used in statistics object."

template <typename T> class matrix;

template <typename T>
matrix<T> operator +(const matrix<T>& leftMat, const matrix<T>& rightMat) noexcept(false);
template <typename T>
matrix<T> operator -(const matrix<T>& leftMat, const matrix<T>& rightMat) noexcept(false);
template <typename T>
matrix<T> operator *(const matrix<T>& leftMat, const T& scaler) noexcept;
template <typename T>
matrix<T> operator *(const matrix<T>& leftMat, const matrix<T>& rightMat) noexcept(false);
template <typename T>
matrix<T> operator *(const matrix<T>& leftMat, const std::vector<T>& rightVect) noexcept(false);
template <typename T>
matrix<T> operator /(const matrix<T>& leftMat, const T& scaler) noexcept;
template <typename T>
matrix<T> operator /(const matrix<T>& leftMat, const matrix<T>& rightMat) noexcept(false);
//template <typename T>
//matrix<T> operator ^(const matrix<T>& leftMat, const size_t& power) noexcept(false);

template <typename T>
matrix<T> matrix_transpose(const matrix<T>& transMat) noexcept;
template <typename T>
matrix<T> matrix_identity(const matrix<T>& eyeMat) noexcept(false);
template <typename T>
matrix<T> matrix_inverse(const matrix<T>& invMat) noexcept(false);
template <typename T>
matrix<T> matrix_triangleUP(const matrix<T>& triuMat) noexcept(false);
template <typename T>
matrix<T> matrix_triangleLOW(const matrix<T>& trilMat) noexcept(false);
template <typename T>
matrix<T> matrix_ref(const matrix<T>& MAT, const std::vector<T>& COLVECT) noexcept(false);
template <typename T>
matrix<T> matrix_rref(const matrix<T>& MAT, const std::vector<T>& COLVECT) noexcept(false);
template <typename T>
matrix<T> matrix_pivot(const matrix<T>& MAT) noexcept(false);

template <typename T> 
float get_determinantf(const matrix<T>& MAT) noexcept(false);
template <typename T> 
double get_determinant(const matrix<T>& MAT) noexcept(false);
template <typename T> 
long double get_determinantl(const matrix<T>& MAT) noexcept(false);

template <typename T>
float get_normf(const matrix<T>& MAT) noexcept(false);
template <typename T>
double get_norm(const matrix<T>& MAT) noexcept(false);
template <typename T>
long double get_norml(const matrix<T>& MAT) noexcept(false);

template <typename T>
std::vector<T> get_eigenValues(const matrix<T>& MAT, const size_t& numIterations = DEFAULT_ITERATIONS) noexcept(false);
template <typename T>
std::vector<std::vector<T>> get_eigenVectors(const matrix<T>& MAT) noexcept(false);

template <typename T>
std::vector<matrix<T>> LU(const matrix<T>& MAT) noexcept(false);
template <typename T>
std::vector<matrix<T>> QR(const matrix<T>& MAT) noexcept(false);

template <typename T>
matrix<T> get_truncate_columns(const matrix<T>& MAT, const size_t& newColumn_size) noexcept(false);
template <typename T>
matrix<T> get_truncate_rows(const matrix<T>& MAT, const size_t& newRow_size) noexcept(false);

template <typename T>
std::vector<T> unit_vector(const std::vector<T>& VECTOR) noexcept(false);
template <typename T>
std::vector<T> vector_projection(const std::vector<T>& A, const std::vector<T>& B) noexcept(false);
template <typename T>
T scaler_projection(const std::vector<T>& A, const std::vector<T>& B) noexcept(false);
template <typename T>
T dot_product(const std::vector<T>& A, const std::vector<T>& B) noexcept(false);
template <typename T>
std::vector<T> cross_product(const std::vector<T>& A, const std::vector<T>& B) noexcept(false);
template <typename T>
std::vector<T> vector_subtraction(const std::vector<T>& A, const std::vector<T>& B) noexcept(false);
template <typename T>
std::vector<T> vector_addition(const std::vector<T>& A, const std::vector<T>& B) noexcept(false);
template <typename T>
std::vector<T> vector_scaler(const std::vector<T>& A, const T& scaler) noexcept(false);
template <typename T>
T vector_magnitude(const std::vector<T>& VECT) noexcept(false);

template <typename T>
void eliminate_floating_epsilon(matrix<T>& floatMat, const T& _EPSILON = EPSILON_NULL) noexcept;

template <typename T>
FORCE_INLINE size_t max_rowIdx(const matrix<T>& MAT) noexcept
{
	return MAT.get_row_size() - 1;
}
template <typename T>
FORCE_INLINE size_t max_columnIdx(const matrix<T>& MAT) noexcept
{
	return MAT.get_column_size() - 1;
}

template <typename T>
matrix<T> round_matrix(const matrix<T>& MAT) noexcept;
template <typename T>
matrix<T> ceil_matrix(const matrix<T>& MAT) noexcept;
template <typename T>
matrix<T> floor_matrix(const matrix<T>& MAT) noexcept;

template <typename T>
matrix<T> vandermonde_matrix(const size_t& rows, const size_t& columns) noexcept;

template <typename T> 
void print_out_matrix(const matrix<T>& MAT) noexcept;

template <typename T>
class matrix
{
	static_assert((std::is_same<T, signed char>::value							||
				   std::is_same<T, std::complex<signed char>>::value			||
				   std::is_same<T, signed short>::value							||
				   std::is_same<T, std::complex<signed short>>::value			||
				   std::is_same<T, signed int>::value							||
				   std::is_same<T, std::complex<signed int>>::value				||
				   std::is_same<T, signed long long>::value						||
				   std::is_same<T, std::complex<signed long long>>::value		||
				   std::is_same<T, float>::value								||
				   std::is_same<T, std::complex<float>>::value					||
				   std::is_same<T, double>::value								||
				   std::is_same<T, std::complex<double>>::value					||
				   std::is_same<T, long double>::value							||
				   std::is_same<T, std::complex<long double>>::value),
				   TYPE_ERROR_MSG);

private:
	size_t m_sizeRow;
	size_t m_sizeColumn;
	T* m_pTMatrix_buff;

	FORCE_INLINE void zero_buffer(void) noexcept;
	FORCE_INLINE void copy_buffer(const T* __restrict copyBuffer) noexcept;
	FORCE_INLINE size_t array_index(const size_t& rowIdx, const size_t& colIdx) const noexcept;

protected:
	static matrix get_aiMatrix(const matrix& ai_from) noexcept(false);
	static void aiMatrix_leftshift(matrix& aiMat) noexcept(false);
	static void copy_matrix_ignore_size(matrix& _Dest, const matrix& _Src) noexcept;

public:
	matrix(const matrix& copyMatrix) noexcept(false);
	explicit matrix(const size_t& row_size, const size_t& col_size) noexcept(false);
	virtual ~matrix(void) noexcept;

	void resize_matrix(const size_t& newRow, const size_t& newColumn) noexcept(false);
	inline void clear(void) noexcept(false)
	{
		this->zero_buffer();
	}

	matrix& operator =(const matrix& copyMatrix) noexcept(false);

	inline T& operator ()(const size_t& rowIdx, const size_t& columnIdx) const noexcept(false)
	{
		if (rowIdx >= m_sizeRow || columnIdx >= m_sizeColumn)
			throw std::out_of_range("Index out of matrix range.");
		return m_pTMatrix_buff[array_index(rowIdx, columnIdx)];
	}

	FORCE_INLINE bool is_square_matrix(void) const noexcept
	{
		return (m_sizeRow == m_sizeColumn) ? true : false;
	}
	FORCE_INLINE size_t get_row_size(void) const noexcept
	{
		return m_sizeRow;
	}
	FORCE_INLINE size_t get_column_size(void) const noexcept
	{
		return m_sizeColumn;
	}

	matrix& operator +=(const matrix& rightMat) noexcept(false);
	matrix& operator -=(const matrix& rightMat) noexcept(false);
	matrix& operator *=(const T& scaler) noexcept;
	matrix& operator *=(const matrix& rightMat) noexcept(false);
	matrix& operator *=(const std::vector<T>& rightVect) noexcept(false);
	matrix& operator /=(const T& scaler) noexcept;
	matrix& operator /=(const matrix& rightMat) noexcept(false);
	//matrix& operator ^=(const size_t& power) noexcept(false);

	void add_row(const size_t& index_row_receiver, const size_t& index_row_adder) noexcept(false);
	void subtract_row(const size_t& index_row_receiver, const size_t& index_row_subtractor) noexcept(false);
	void multiply_row(const size_t& index_row_receiver, const T& multiplier) noexcept(false);
	void interchange_rows(const size_t& index_row_upper, const size_t& index_row_lower) noexcept(false);

	void transpose(void) noexcept;
	void identity(void) noexcept(false);
	void inverse(void) noexcept(false);
	void triangleUP(void) noexcept(false);
	void triangleLOW(void) noexcept(false);

	std::vector<T> row_to_vector(const size_t& rowIndex) const noexcept(false);
	std::vector<T> column_to_vector(const size_t& columnIndex) const noexcept(false);
	std::vector<T> upper_diag_to_vector(void) const noexcept;
	std::vector<T> lower_diag_to_vector(void) const noexcept;
	void vector_to_row(const size_t& rowIndex, const std::vector<T>& vectIN) noexcept(false);
	void vector_to_column(const size_t& coumnIndex, const std::vector<T>& vectIN) noexcept(false);
	void vector_to_upper_diag(const std::vector<T>& diaVect) noexcept(false);
	void vector_to_lower_diag(const std::vector<T>& diaVect) noexcept(false);

	void truncate_columns(const size_t& newColumn_size) noexcept(false);
	void truncate_rows(const size_t& newRow_size) noexcept(false);

	friend matrix operator +<T>(const matrix& leftMat, const matrix& rightMat) noexcept(false);
	friend matrix operator -<T>(const matrix& leftMat, const matrix& rightMat) noexcept(false);
	friend matrix operator *<T>(const matrix& leftMat, const T& scaler) noexcept;
	friend matrix operator *<T>(const matrix& leftMat, const matrix& rightMat) noexcept(false);

	friend matrix operator *<T>(const matrix& leftMat, const std::vector<T>& rightVect) noexcept(false);

	friend matrix operator /<T>(const matrix& leftMat, const T& scaler) noexcept;
	friend matrix operator /<T>(const matrix& leftMat, const matrix& rightMat) noexcept(false);
	//friend matrix operator ^<T>(const matrix& leftMat, const size_t& power) noexcept(false);

	friend matrix matrix_transpose<T>(const matrix& transMat) noexcept;
	friend matrix matrix_identity<T>(const matrix& eyeMat) noexcept(false);
	friend matrix matrix_inverse<T>(const matrix& invMat) noexcept(false);
	friend matrix matrix_triangleUP<T>(const matrix& triuMat) noexcept(false);
	friend matrix matrix_triangleLOW<T>(const matrix& trilMat) noexcept(false);
	friend matrix matrix_ref<T>(const matrix& MAT, const std::vector<T>& COLVECT) noexcept(false);
	friend matrix matrix_rref<T>(const matrix& MAT, const std::vector<T>& COLVECT) noexcept(false);
	friend matrix matrix_pivot<T>(const matrix& MAT) noexcept(false);

	friend float get_determinantf<T>(const matrix& MAT) noexcept(false);
	friend double get_determinant<T>(const matrix& MAT) noexcept(false);
	friend long double get_determinantl<T>(const matrix& MAT) noexcept(false);

	friend float get_normf<T>(const matrix& MAT) noexcept(false);
	friend double get_norm<T>(const matrix& MAT) noexcept(false);
	friend long double get_norml<T>(const matrix& MAT) noexcept(false);

	friend std::vector<T> get_eigenValues<T>(const matrix& MAT, const size_t& numIterations) noexcept(false);
	friend std::vector<std::vector<T>> get_eigenVectors<T>(const matrix& MAT) noexcept(false);

	friend std::vector<matrix> LU<T>(const matrix& MAT) noexcept(false);
	friend std::vector<matrix> QR<T>(const matrix& MAT) noexcept(false);

	friend matrix get_truncate_columns<T>(const matrix& MAT, const size_t& newColumn_size) noexcept(false);
	friend matrix get_truncate_rows<T>(const matrix& MAT, const size_t& newRow_size) noexcept(false);

	friend void print_out_matrix<T>(const matrix& MAT) noexcept;
};

#endif //__MATRIX_H__
