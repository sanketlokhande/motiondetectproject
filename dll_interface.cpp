#include "dll_interface.h"

extern "C" __declspec(dllexport) MOTION_HANDLE motion_init(unsigned int _u4Sample_size) noexcept
{
	try {
		project_motionDetect* __ABSTRACT(new project_motionDetect(_u4Sample_size));
		return reinterpret_cast<MOTION_HANDLE>(__ABSTRACT);
	}
	catch (const std::bad_alloc&)
	{
		return nullptr;
	}
}

extern "C" __declspec(dllexport) void motion_free(MOTION_HANDLE& __handle) noexcept
{
	if (__handle)
	{
		delete reinterpret_cast<project_motionDetect*>(__handle);
		__handle = nullptr;
	}
}

extern "C" __declspec(dllexport) BYTE motion_process(const MOTION_HANDLE __handle, project_motionDetect::project_motionData* __buffer) noexcept
{
	return(__handle) ? reinterpret_cast<project_motionDetect*>(__handle)->process_samples(__buffer) : static_cast<BYTE>(NULL);
}

extern "C" __declspec(dllexport) void motion_first_run(const MOTION_HANDLE __handle, project_motionDetect::project_motionData* __buffer) noexcept
{
	if (__handle)
		reinterpret_cast<project_motionDetect*>(__handle)->run_once_at_start(__buffer);
}