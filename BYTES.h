#pragma once
#ifndef __BYTES_H__
#define __BYTES_H__
#include "BaseStruct.h"

union BYTE;

BYTE operator &(const BYTE& thisByte, const unsigned char& ANDWITH) noexcept;
BYTE operator &(const BYTE& thisByte, const BYTE& ANDWITH) noexcept;
BYTE operator |(const BYTE& thisByte, const unsigned char& ORWITH) noexcept;
BYTE operator |(const BYTE& thisByte, const BYTE& ORWITH) noexcept;
BYTE operator ^(const BYTE& thisByte, const unsigned char& XORWITH) noexcept;
BYTE operator ^(const BYTE& thisByte, const BYTE& XORWITH) noexcept;
BYTE operator <<(const BYTE& thisByte, const size_t& LEFTSHIFTAMOUNT) noexcept;
BYTE operator >>(const BYTE& thisByte, const size_t& RIGHTSHIFTAMOUNT) noexcept;

BYTE operator +(const BYTE& thisbyte, const BYTE& adder) noexcept;
BYTE operator -(const BYTE& thisbyte, const BYTE& subtractor) noexcept;
BYTE operator *(const BYTE& thisbyte, const BYTE& multiplier) noexcept;
BYTE operator /(const BYTE& thisbyte, const BYTE& divider) noexcept;

bool operator ==(const BYTE& leftByte, const BYTE& rightByte) noexcept;
bool operator !=(const BYTE& leftByte, const BYTE& rightByte) noexcept;

union alignas(unsigned char) BYTE final
{
	BYTE_FIELD BITS;
	unsigned char BYTES;

	BYTE(const BYTE & COPY) noexcept;
	BYTE(BYTE && MOVE) noexcept;
	BYTE(const unsigned char& BYTECOPY) noexcept;
	BYTE(unsigned char&& BYTEMOVE) noexcept;
	BYTE(void) noexcept;
	~BYTE(void) noexcept = default;

	BYTE& operator =(const BYTE & COPY) noexcept;
	BYTE& operator =(BYTE && MOVE) noexcept;
	BYTE& operator =(const unsigned char& BYTECOPY) noexcept;
	BYTE& operator =(unsigned char&& BYTEMOVE) noexcept;

	BYTE& operator ()(const size_t & BITINDEX, const SINGLE_BIT & BIT) noexcept;
	inline void setBit(const size_t& BITINDEX, const SINGLE_BIT& BIT) noexcept
	{
		this->operator()(BITINDEX, BIT);
	}

	BYTE& operator &=(const unsigned char& ANDWITH) noexcept;
	BYTE& operator &=(const BYTE& ANDWITH) noexcept;
	BYTE& operator |=(const unsigned char& ORWITH) noexcept;
	BYTE& operator |=(const BYTE& ORWITH) noexcept;
	BYTE& operator ^=(const unsigned char& XORWITH) noexcept;
	BYTE& operator ^=(const BYTE& XORWITH) noexcept;
	BYTE& operator <<=(const size_t& LEFTSHIFTAMOUNT) noexcept;
	BYTE& operator >>=(const size_t& RIGHTSHIFTAMOUNT) noexcept;
	BYTE& operator ~(void) noexcept;

	BYTE& operator +=(const BYTE& added) noexcept;
	BYTE& operator -=(const BYTE& subtracted) noexcept;
	BYTE& operator *=(const BYTE& multiply) noexcept;
	BYTE& operator /=(const BYTE& divided) noexcept;

	template <typename T>
	BYTE& operator +=(const T& typeAdded) noexcept
	{
		BYTES += static_cast<unsigned char>(typeAdded);
		return (*this);
	}
	template <typename T>
	BYTE& operator -=(const T& typeSubtracted) noexcept
	{
		BYTES -= static_cast<unsigned char>(typeSubtracted);
		return (*this);
	}
	template <typename T>
	BYTE& operator *=(const T& typeMultiplied) noexcept
	{
		BYTES *= static_cast<unsigned char>(typeMultiplied);
		return (*this);
	}
	template <typename T>
	BYTE& operator /=(const T& typeDivided) noexcept
	{
		BYTES /= static_cast<unsigned char>(typeDivided);
		return (*this);
	}

	bool operator ==(const BYTE& checkByte) noexcept;
	bool operator !=(const BYTE& checkByte) noexcept;

	template <typename T>
	bool operator ==(const T& type) noexcept;
	template <typename T>
	bool operator !=(const T& type) noexcept;

	friend BYTE operator &(const BYTE& thisByte, const unsigned char& ANDWITH) noexcept;
	friend BYTE operator &(const BYTE& thisByte, const BYTE& ANDWITH) noexcept;
	friend BYTE operator |(const BYTE& thisByte, const unsigned char& ORWITH) noexcept;
	friend BYTE operator |(const BYTE& thisByte, const BYTE& ORWITH) noexcept;
	friend BYTE operator ^(const BYTE& thisByte, const unsigned char& XORWITH) noexcept;
	friend BYTE operator ^(const BYTE& thisByte, const BYTE& XORWITH) noexcept;
	friend BYTE operator <<(const BYTE& thisByte, const size_t& LEFTSHIFTAMOUNT) noexcept;
	friend BYTE operator >>(const BYTE& thisByte, const size_t& RIGHTSHIFTAMOUNT) noexcept;

	friend BYTE operator +(const BYTE& thisbyte, const BYTE& adder) noexcept;
	friend BYTE operator -(const BYTE& thisbyte, const BYTE& subtractor) noexcept;
	friend BYTE operator *(const BYTE& thisbyte, const BYTE& multiplier) noexcept;
	friend BYTE operator /(const BYTE& thisbyte, const BYTE& divider) noexcept;

	friend bool operator ==(const BYTE& leftByte, const BYTE& rightByte) noexcept;
	friend bool operator !=(const BYTE& leftByte, const BYTE& rightByte) noexcept;

	template <typename T>
	friend bool operator ==(const BYTE& leftByte, const T& type) noexcept;
	template <typename T>
	friend bool operator !=(const BYTE& leftByte, const T& type) noexcept;

	template <typename T>
	friend BYTE operator +(const BYTE& thisbyte, const T& adder) noexcept;
	template <typename T>
	friend BYTE operator -(const BYTE& thisbyte, const T& subtractor) noexcept;
	template <typename T>
	friend BYTE operator *(const BYTE& thisbyte, const T& multiplier) noexcept;
	template <typename T>
	friend BYTE operator /(const BYTE& thisbyte, const T& divider) noexcept;
};

template <typename T>
BYTE operator +(const BYTE& thisbyte, const T& adder) noexcept
{
	BYTE _RET(thisbyte.BYTES + static_cast<unsigned char>(adder));
	return _RET;
}
template <typename T>
BYTE operator -(const BYTE& thisbyte, const T& subtractor) noexcept
{
	BYTE _RET(thisbyte.BYTES - static_cast<unsigned char>(subtractor));
	return _RET;
}
template <typename T>
BYTE operator *(const BYTE& thisbyte, const T& multiplier) noexcept
{
	BYTE _RET(thisbyte.BYTES * static_cast<unsigned char>(multiplier));
	return _RET;
}
template <typename T>
BYTE operator /(const BYTE& thisbyte, const T& divider) noexcept
{
	BYTE _RET(thisbyte.BYTES / static_cast<unsigned char>(divider));
	return _RET;
}

template <typename T>
bool BYTE::operator ==(const T& type)noexcept
{
	return BYTES == static_cast<unsigned char>(type);
}
template <typename T>
bool BYTE::operator !=(const T& type)noexcept
{
	return BYTES != static_cast<unsigned char>(type);
}

template <typename T>
bool operator ==(const BYTE& leftByte, const T& type) noexcept
{
	return leftByte.BYTES == static_cast<unsigned char>(type);
}
template <typename T>
bool operator !=(const BYTE& leftByte, const T& type) noexcept
{
	return leftByte.BYTES != static_cast<unsigned char>(type);
}

#endif //__BYTES_H__