#include "project_circBuffer.h"

#ifndef __project_CIRCBUFFER_CPP__
#define __project_CIRCBUFFER_CPP__

template <typename data_t>
FORCE_INLINE void project_circBuffer<data_t>::increment_store_idx(void) const
{
	if ((m_u4Store_idx + 1) == m_u4Load_idx)
		this->increment_load_idx();

	if ((m_u4Store_idx + 1) < this->m_u4Buffer_size)
		m_u4Store_idx++;
	else
	{
		if (m_u4Load_idx == 0)
			this->increment_load_idx();
		m_u4Store_idx = 0;
	}
}
template <typename data_t>
FORCE_INLINE void project_circBuffer<data_t>::increment_load_idx(void) const
{
	if ((m_u4Load_idx + 1) != m_u4Store_idx && m_u4Load_idx != m_u4Store_idx)
	{
		if ((m_u4Load_idx + 1) < this->m_u4Buffer_size)
			m_u4Load_idx++;
		else
		{
			if (m_u4Store_idx != 0)
				m_u4Load_idx = 0;
		}
	}
}

template <typename data_t>
project_circBuffer<data_t>::project_circBuffer(unsigned int _u4Size) noexcept
: project_OBJECT<data_t>(_u4Size, NUMBER_OF_BUFFERS)
, m_u4Load_idx(0)
, m_u4Store_idx(0)
{

}
template <typename data_t>
project_circBuffer<data_t>::~project_circBuffer(void)
{

}

template <typename data_t>
data_t& project_circBuffer<data_t>::operator[](unsigned int _u4Idx) noexcept
{
	unsigned int _u4Difference = 0;

	if (m_u4Load_idx < m_u4Store_idx)
		return (m_u4Load_idx + _u4Idx < m_u4Store_idx) ? this->m_pData_buffer[MAIN_BUFFER][m_u4Load_idx + _u4Idx] : this->m_pData_buffer[MAIN_BUFFER][m_u4Store_idx];
	else
	{
		if (m_u4Load_idx + _u4Idx < this->m_u4Buffer_size)
			return this->m_pData_buffer[MAIN_BUFFER][m_u4Load_idx + _u4Idx];
		else
		{
			_u4Difference = m_u4Load_idx + _u4Idx - this->m_u4Buffer_size;
			return (_u4Difference < m_u4Store_idx) ? this->m_pData_buffer[MAIN_BUFFER][_u4Difference] : this->m_pData_buffer[MAIN_BUFFER][m_u4Store_idx];
		}
	}
}

template <typename data_t>
void project_circBuffer<data_t>::add_data(const data_t &_DATA) const noexcept
{
	this->m_pData_buffer[MAIN_BUFFER][m_u4Store_idx] = _DATA;
	this->increment_store_idx();
}
template <typename data_t>
data_t& project_circBuffer<data_t>::get_data(void) const noexcept
{
	unsigned int return_idx = m_u4Load_idx;
	this->increment_load_idx();
	return this->m_pData_buffer[MAIN_BUFFER][return_idx];
}
template <typename data_t>
unsigned int project_circBuffer<data_t>::size(void) const noexcept
{
	unsigned int _difference = 0;

	if (m_u4Store_idx > m_u4Load_idx)
		_difference = m_u4Store_idx - m_u4Load_idx;
	else
	{
		_difference = m_u4Store_idx;
		_difference += (this->m_u4Buffer_size - m_u4Load_idx);
	}

	return _difference;
}
template <typename data_t>
void project_circBuffer<data_t>::reset(void)
{
	project_ZeroMemory<data_t>(this->m_pData_buffer[MAIN_BUFFER], this->m_u4Buffer_size);
}
#endif //__project_CIRCBUFFER_CPP__
