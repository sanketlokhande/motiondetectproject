#pragma once
#ifndef __BASESTRUCT_H__
#define __BASESTRUCT_H__
#include <cstdlib>

#ifdef TRUE
#undef TRUE
#endif
#define TRUE (NULL == NULL)

#ifdef FALSE
#undef FALSE
#endif
#define FALSE !TRUE


using SINGLE_BIT = unsigned char;

constexpr SINGLE_BIT LOGIC_HIGH = TRUE;
constexpr SINGLE_BIT LOGIC_LOW = !LOGIC_HIGH;

struct alignas(unsigned char) BYTE_FIELD final
{
	unsigned char BIT0 : 1,
		BIT1 : 1,
		BIT2 : 1,
		BIT3 : 1,
		BIT4 : 1,
		BIT5 : 1,
		BIT6 : 1,
		BIT7 : 1;

	BYTE_FIELD(const BYTE_FIELD&) = delete;
	BYTE_FIELD(BYTE_FIELD&&) = delete;
	explicit BYTE_FIELD(void) = default;
	~BYTE_FIELD(void) = default;

	BYTE_FIELD& operator =(const BYTE_FIELD&) = delete;
	BYTE_FIELD& operator =(BYTE_FIELD&&) = delete;
};

struct alignas(unsigned short) HWORD_FIELD final
{
	unsigned short BIT0 : 1,
		BIT1 : 1,
		BIT2 : 1,
		BIT3 : 1,
		BIT4 : 1,
		BIT5 : 1,
		BIT6 : 1,
		BIT7 : 1,
		BIT8 : 1,
		BIT9 : 1,
		BIT10 : 1,
		BIT11 : 1,
		BIT12 : 1,
		BIT13 : 1,
		BIT14 : 1,
		BIT15 : 1;

	HWORD_FIELD(const HWORD_FIELD&) = delete;
	HWORD_FIELD(HWORD_FIELD&&) = delete;
	explicit HWORD_FIELD(void) = default;
	~HWORD_FIELD(void) = default;

	HWORD_FIELD& operator =(const HWORD_FIELD&) = delete;
	HWORD_FIELD& operator =(HWORD_FIELD&&) = delete;
};

struct alignas(unsigned int) WORD_FIELD final
{
	unsigned int BIT0 : 1,
		BIT1 : 1,
		BIT2 : 1,
		BIT3 : 1,
		BIT4 : 1,
		BIT5 : 1,
		BIT6 : 1,
		BIT7 : 1,
		BIT8 : 1,
		BIT9 : 1,
		BIT10 : 1,
		BIT11 : 1,
		BIT12 : 1,
		BIT13 : 1,
		BIT14 : 1,
		BIT15 : 1,
		BIT16 : 1,
		BIT17 : 1,
		BIT18 : 1,
		BIT19 : 1,
		BIT20 : 1,
		BIT21 : 1,
		BIT22 : 1,
		BIT23 : 1,
		BIT24 : 1,
		BIT25 : 1,
		BIT26 : 1,
		BIT27 : 1,
		BIT28 : 1,
		BIT29 : 1,
		BIT30 : 1,
		BIT31 : 1;

	WORD_FIELD(const WORD_FIELD&) = delete;
	WORD_FIELD(WORD_FIELD&&) = delete;
	explicit WORD_FIELD(void) = default;
	~WORD_FIELD(void) = default;

	WORD_FIELD& operator =(const WORD_FIELD&) = delete;
	WORD_FIELD& operator =(WORD_FIELD&&) = delete;
};

struct alignas(unsigned long long) DWORD_FIELD final
{
	unsigned long long BIT0 : 1,
		BIT1 : 1,
		BIT2 : 1,
		BIT3 : 1,
		BIT4 : 1,
		BIT5 : 1,
		BIT6 : 1,
		BIT7 : 1,
		BIT8 : 1,
		BIT9 : 1,
		BIT10 : 1,
		BIT11 : 1,
		BIT12 : 1,
		BIT13 : 1,
		BIT14 : 1,
		BIT15 : 1,
		BIT16 : 1,
		BIT17 : 1,
		BIT18 : 1,
		BIT19 : 1,
		BIT20 : 1,
		BIT21 : 1,
		BIT22 : 1,
		BIT23 : 1,
		BIT24 : 1,
		BIT25 : 1,
		BIT26 : 1,
		BIT27 : 1,
		BIT28 : 1,
		BIT29 : 1,
		BIT30 : 1,
		BIT31 : 1,
		BIT32 : 1,
		BIT33 : 1,
		BIT34 : 1,
		BIT35 : 1,
		BIT36 : 1,
		BIT37 : 1,
		BIT38 : 1,
		BIT39 : 1,
		BIT40 : 1,
		BIT41 : 1,
		BIT42 : 1,
		BIT43 : 1,
		BIT44 : 1,
		BIT45 : 1,
		BIT46 : 1,
		BIT47 : 1,
		BIT48 : 1,
		BIT49 : 1,
		BIT50 : 1,
		BIT51 : 1,
		BIT52 : 1,
		BIT53 : 1,
		BIT54 : 1,
		BIT55 : 1,
		BIT56 : 1,
		BIT57 : 1,
		BIT58 : 1,
		BIT59 : 1,
		BIT60 : 1,
		BIT61 : 1,
		BIT62 : 1,
		BIT63 : 1;

	DWORD_FIELD(const DWORD_FIELD&) = delete;
	DWORD_FIELD(DWORD_FIELD&&) = delete;
	explicit DWORD_FIELD(void) = default;
	~DWORD_FIELD(void) = default;

	DWORD_FIELD& operator =(const DWORD_FIELD&) = delete;
	DWORD_FIELD& operator =(DWORD_FIELD&&) = delete;
};


#endif //__BASESTRUCT_H__ 