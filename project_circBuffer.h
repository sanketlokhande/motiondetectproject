#pragma once
#ifndef __project_CIRCBUFFER_H__
#define __project_CIRCBUFFER_H__

#include "project_defs.h"
#include "project_objects.h"
#include "project_exception.h"

template <typename data_t>
class DLL_PRIVATE project_circBuffer final : private project_OBJECT<data_t>
{
	enum BUFFER_ID : unsigned int
	{
		MAIN_BUFFER,
		NUMBER_OF_BUFFERS
	};

private:
	mutable unsigned int m_u4Load_idx;
	mutable unsigned int m_u4Store_idx;

	FORCE_INLINE void increment_store_idx(void) const;
	FORCE_INLINE void increment_load_idx(void) const;

public:
	BLOCK_OBJECT_COPY_AND_MOVE(project_circBuffer);

	explicit project_circBuffer(unsigned int _u4Size) noexcept;
	virtual ~project_circBuffer(void);

	data_t& operator[](unsigned int _u4Idx) noexcept;

	void add_data(const data_t &_DATA) const noexcept;
	data_t& get_data(void) const noexcept;
	unsigned int size(void) const noexcept;
	void reset(void) override;
};
#endif //__project_CIRCBUFFER_H__