#include "BYTES.h"

BYTE::BYTE(const BYTE& COPY) noexcept
	: BYTES(COPY.BYTES)
{

}
BYTE::BYTE(BYTE&& MOVE) noexcept
	: BYTES(MOVE.BYTES)
{
	MOVE.BYTES = static_cast<unsigned char>(NULL);
}
BYTE::BYTE(const unsigned char& BYTECOPY) noexcept
	: BYTES(BYTECOPY)
{

}
BYTE::BYTE(unsigned char&& BYTEMOVE) noexcept
	: BYTES(BYTEMOVE)
{
	BYTEMOVE = static_cast<unsigned char>(NULL);
}
BYTE::BYTE(void) noexcept
	: BYTES(static_cast<unsigned char>(NULL))
{

}

BYTE& BYTE::operator =(const BYTE& COPY) noexcept
{
	this->BYTES = COPY.BYTES;
	return (*this);
}
BYTE& BYTE::operator =(BYTE&& MOVE) noexcept
{
	this->BYTES = MOVE.BYTES;
	MOVE.BYTES = static_cast<unsigned char>(NULL);
	return (*this);
}
BYTE& BYTE::operator =(const unsigned char& BYTECOPY) noexcept
{
	BYTES = BYTECOPY;
	return (*this);
}
BYTE& BYTE::operator =(unsigned char&& BYTEMOVE) noexcept
{
	BYTES = BYTEMOVE;
	BYTEMOVE = static_cast<unsigned char>(NULL);
	return (*this);
}

BYTE& BYTE::operator ()(const size_t& BITINDEX, const SINGLE_BIT& BIT) noexcept
{
	switch (BITINDEX)
	{
	case 0: BITS.BIT0 = BIT; break;
	case 1: BITS.BIT1 = BIT; break;
	case 2: BITS.BIT2 = BIT; break;
	case 3: BITS.BIT3 = BIT; break;
	case 4: BITS.BIT4 = BIT; break;
	case 5: BITS.BIT5 = BIT; break;
	case 6: BITS.BIT6 = BIT; break;
	case 7: BITS.BIT7 = BIT; break;
	}
	return (*this);
}

BYTE& BYTE::operator &=(const unsigned char& ANDWITH) noexcept
{
	BYTES &= ANDWITH;
	return (*this);
}
BYTE& BYTE::operator &=(const BYTE& ANDWITH) noexcept
{
	this->BYTES &= ANDWITH.BYTES;
	return (*this);
}
BYTE& BYTE::operator |=(const unsigned char& ORWITH) noexcept
{
	BYTES |= ORWITH;
	return (*this);
}
BYTE& BYTE::operator |=(const BYTE& ORWITH) noexcept
{
	this->BYTES |= ORWITH.BYTES;
	return (*this);
}
BYTE& BYTE::operator ^=(const unsigned char& XORWITH) noexcept
{
	BYTES ^= XORWITH;
	return (*this);
}
BYTE& BYTE::operator ^=(const BYTE& XORWITH) noexcept
{
	this->BYTES ^= XORWITH.BYTES;
	return (*this);
}
BYTE& BYTE::operator <<=(const size_t& LEFTSHIFTAMOUNT) noexcept
{
	BYTES <<= LEFTSHIFTAMOUNT;
	return (*this);
}
BYTE& BYTE::operator >>=(const size_t& RIGHTSHIFTAMOUNT) noexcept
{
	BYTES <<= RIGHTSHIFTAMOUNT;
	return (*this);
}
BYTE& BYTE::operator ~(void) noexcept
{
	BYTES = ~BYTES;
	return (*this);
}

BYTE& BYTE::operator +=(const BYTE& added) noexcept
{
	this->BYTES += added.BYTES;
	return (*this);
}
BYTE& BYTE::operator -=(const BYTE& subtracted) noexcept
{
	this->BYTES -= subtracted.BYTES;
	return (*this);
}
BYTE& BYTE::operator *=(const BYTE& multiply) noexcept
{
	this->BYTES *= multiply.BYTES;
	return (*this);
}
BYTE& BYTE::operator /=(const BYTE& divided) noexcept
{
	this->BYTES /= divided.BYTES;
	return (*this);
}

bool BYTE::operator ==(const BYTE& checkByte) noexcept
{
	return this->BYTES == checkByte.BYTES;
}
bool BYTE::operator !=(const BYTE& checkByte) noexcept
{
	return this->BYTES != checkByte.BYTES;
}


BYTE operator &(const BYTE& thisByte, const unsigned char& ANDWITH) noexcept
{
	BYTE _RET(thisByte.BYTES & ANDWITH);
	return _RET;
}
BYTE operator &(const BYTE& thisByte, const BYTE& ANDWITH) noexcept
{
	BYTE _RET(thisByte.BYTES & ANDWITH.BYTES);
	return _RET;
}
BYTE operator |(const BYTE& thisByte, const unsigned char& ORWITH) noexcept
{
	BYTE _RET(thisByte.BYTES | ORWITH);
	return _RET;
}
BYTE operator |(const BYTE& thisByte, const BYTE& ORWITH) noexcept
{
	BYTE _RET(thisByte.BYTES | ORWITH.BYTES);
	return _RET;
}
BYTE operator ^(const BYTE& thisByte, const unsigned char& XORWITH) noexcept
{
	BYTE _RET(thisByte.BYTES ^ XORWITH);
	return _RET;
}
BYTE operator ^(const BYTE& thisByte, const BYTE& XORWITH) noexcept
{
	BYTE _RET(thisByte.BYTES ^ XORWITH.BYTES);
	return _RET;
}
BYTE operator <<(const BYTE& thisByte, const size_t& LEFTSHIFTAMOUNT) noexcept
{
	BYTE _RET(thisByte.BYTES << LEFTSHIFTAMOUNT);
	return _RET;
}
BYTE operator >>(const BYTE& thisByte, const size_t& RIGHTSHIFTAMOUNT) noexcept
{
	BYTE _RET(thisByte.BYTES >> RIGHTSHIFTAMOUNT);
	return _RET;
}

BYTE operator +(const BYTE& thisbyte, const BYTE& adder) noexcept
{
	BYTE _RET(thisbyte.BYTES + adder.BYTES);
	return _RET;
}
BYTE operator -(const BYTE& thisbyte, const BYTE& subtractor) noexcept
{
	BYTE _RET(thisbyte.BYTES - subtractor.BYTES);
	return _RET;
}
BYTE operator *(const BYTE& thisbyte, const BYTE& multiplier) noexcept
{
	BYTE _RET(thisbyte.BYTES * multiplier.BYTES);
	return _RET;
}
BYTE operator /(const BYTE& thisbyte, const BYTE& divider) noexcept
{
	BYTE _RET(thisbyte.BYTES / divider.BYTES);
	return _RET;
}

bool operator ==(const BYTE& leftByte, const BYTE& rightByte) noexcept
{
	return leftByte.BYTES == rightByte.BYTES;
}
bool operator !=(const BYTE& leftByte, const BYTE& rightByte) noexcept
{
	return leftByte.BYTES != rightByte.BYTES;
}