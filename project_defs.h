#pragma once
#ifndef __project_DEFS_H__
#define __project_DEFS_H__
#include <stdlib.h>
#include <math.h>
#include <complex>
#include <vector>
#include <numeric>
#include <cstdarg>
#include <string.h>

#define BITS_PER_BYTE									8U
#define _P(ptr)											(*ptr)
#define TEMPLATE_SIZE									200U

constexpr double SECONDS_PER_MINUTE						= 60.00;		// [seconds/minute]
constexpr double MILLISECONDS_PER_SECOND				= 1000.00;		// [milli-seconds/seconds]
constexpr double ONE_HUNDRED_PRECENT					= 100.00;		// %

static const float SINGLE_PRECISION_PI					= (atanf(1.0F) * 4.0F);
static const double DOUBLE_PRECISION_PI					= (atan(1.0) * 4.0);
static const long double QUADRUPLE_PRECISION_PI			= (atanl(1.0L) * 4.0L);

#ifdef __cplusplus
	using project_HANDLE										= size_t;
#else 
	typedef size_t											project_HANDLE;
#endif

#if INTPTR_MAX == INT32_MAX
	#define __32BITS__
	using SINT = signed int;
	using UINT = unsigned int;
#elif INTPTR_MAX == INT64_MAX
	#define __64BITS__ 
	using SINT = signed long long;
	using UINT = unsigned long long;
#else
	#error "Architect is not defined as 32Bits or 64Bits."
#endif

#if defined _MSC_VER
	#define __WINDOWS__
	#define FORCE_INLINE									__forceinline
	#define DLL_PUBLIC										__declspec(dllexport)
	#define DLL_PRIVATE										/*place holder for visual c/c++ compiler. VC does not support dll visibility but by default has them hidden. */
	#define STRUCT_ALIGN(byte_amount)						__declspec(align(byte_amount * BITS_PER_BYTE))
	#if defined _M_IX86 || _M_X64
		#define THIS_CALL									__thiscall													//x86-64 calling convention
		#define FAST_CALL									__fastcall													//x86-64 calling convention
		#define SIMD_CALL									__vectorcall												//x86-64 calling convention
		#define PASCAL_CALL									__stdcall													//x86-64 calling convention
		#define C_CALL										__cdecl														//x86-64 calling convention
	#elif defined (_M_ARM)
		#define NAKED_CALL									__declspec(naked)											//ARM calling convention
	#else
		#error "Could not detect architecture of CPU."
	#endif
#elif defined __GNUC__
	#pragma GCC diagnostic ignored "-Wnonnull"
	#define __LINUX__			
	#define FORCE_INLINE									__attribute__((always_inline)) inline
	#define DLL_PUBLIC										__attribute__((visibility ("default")))
	#define DLL_PRIVATE										__attribute__((visibility ("hidden")))
	#define STRUCT_ALIGN(byte_amount)						__attribute__((aligned(byte_amount * BITS_PER_BYTE)))
	#if defined __i386__ || __ia64__
		#define THIS_CALL									__attribute__((thiscall))									//x86-64 calling convention
		#define FAST_CALL									__attribute__((fastcall))									//x86-64 calling convention
		#define SIMD_CALL									__attribute__((sseregparm))									//x86-64 calling convention
		#define PASCAL_CALL									__attribute__((stdcall))									//x86-64 calling convention
		#define C_CALL										__attribute__((cdecl))										//x86-64 calling convention
	#elif defined (__arm__)
		#define NAKED_CALL									__attribute__((naked))										//ARM calling convention
	#else
		#error "Could not detect architecture of CPU."
	#endif
#else
	#error "Could not identify compiler. Please use Visual C/C++ or GNU Compilers."
#endif

#ifdef _DEBUG
	#ifdef __WINDOWS__
		#define _CRTDBG_MAP_ALLOC
		#include <stdlib.h>
		#include <crtdbg.h>
		#define new	new(_CLIENT_BLOCK , __FILE__ , __LINE__)
		#define DO_MEMORY_LEAK_DETECTION(void)					\
				int validation = _CrtDumpMemoryLeaks();			//\
				//if(validation)									\
				//throw std::runtime_error("MEMORY LEAK")
	#endif
#endif

#define BLOCK_OBJECT_COPY_CONSTRUCTOR(object)								\
	public: explicit object(const object &) noexcept = delete
#define BLOCK_OBJECT_COPY_OPERATOR(object)									\
	public: object& operator =(const object &) const noexcept = delete
#define BLOCK_OBJECT_MOVE_CONSTRUCTOR(object)								\
	public: explicit object(const object &&) noexcept = delete
#define BLOCK_OBJECT_MOVE_OPERATOR(object)									\
	public: object& operator =(const object &&) const noexcept = delete
#define BLOCK_OBJECT_COPY_AND_MOVE(object)									\
	BLOCK_OBJECT_COPY_CONSTRUCTOR(object);									\
	BLOCK_OBJECT_COPY_OPERATOR(object);										\
	BLOCK_OBJECT_MOVE_CONSTRUCTOR(object);									\
	BLOCK_OBJECT_MOVE_OPERATOR(object)

template <typename T>
constexpr inline void project_MemoryCopy(T* __restrict _dst, const T* __restrict _src, const size_t& _Size) noexcept
{
	memcpy(_dst, _src, sizeof(T) * _Size);
}

template <typename T>
constexpr inline void project_ZeroMemory(T* __restrict _dst, const size_t& _Size) noexcept
{
	memset(_dst, 0, sizeof(T) * _Size);
}

template <typename T>
constexpr inline T project_mean(const T* __restrict _data, const size_t& _amount) noexcept
{
	return std::accumulate(_data, _data + _amount, static_cast<T>(0)) / static_cast<T>(_amount);
}

#endif //__project_DEFS_H__
