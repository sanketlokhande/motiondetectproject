#pragma once
#ifndef __PROJECT_ERRORCODES_H__
#define __PROJECT_ERRORCODES_H__

namespace PROJECT_ERRORDEFS
{
	enum class PROJECT_ERROR : unsigned int 
	{
		PROJECT_SUCCESS,															// 0

		//PROJECT_OBJECT Object error codes:
		PROJECT_OBJECT_FAILED_TO_ALLOCATE_MEMORY_FOR_BUFFER,						// 1

		//PROJECT_CARDIACMETRICS Object error codes:
		PROJECT_CARDIACMETRICS_FAILED_TO_ALLOCATE_INDEX_LOCATION_VECTOR,			// 2
		PROJECT_CARDIACMETRICS_FAILED_TO_ALLOCATE_HR_CIRCULAR_BUFFER,				// 3
		PROJECT_CARDIACMETRICS_FAILED_TO_ALLOCATE_HRV_CIRCULAR_BUFFER,				// 4

		//PROJECT_CROSSCORRELATION Object error codes:
		PROJECT_CROSSCORRELATION_FAILED_TO_ALLOCATE_MAGNITUDE_BUFFER,				// 5
		PROJECT_CROSSCORRELATION_FAILED_TO_ALLOCATE_RECYCLE_BUFFER,				// 6

		//PROJECT_FFT Object error codes:
		PROJECT_FFT_FAILED_TO_ALLOCATE_TEMP_BUFFER,								// 7

		//PROJECT_FILTER Object error codes:
		PROJECT_FILTER_FAILED_TO_ALLOCATE_ACOEFF_VECTOR_OBJECT,					// 8
		PROJECT_FILTER_FAILED_TO_ALLOCATE_BCOEFF_VECTOR_OBJECT,					// 9
		PROJECT_FILTER_FAILED_TO_ALLOCATE_ANALOGPOLE_VECTOR_OBJECT,				// 10
		PROJECT_FILTER_FAILED_TO_ALLOCATE_DIGITALPOLE_VECTOR_OBJECT,				// 11
		PROJECT_FILTER_FAILED_TO_ALLOCATE_ZEROVALUES_VECTOR_OBJECT,				// 12
		PROJECT_FILTER_FAILED_TO_ALLOCATE_PARAM_LOCK,								// 13

		//PROJECT_ADPWIENERFILTER Object error codes:
		PROJECT_ADPWIENERFILTER_FAILED_TO_ALLOCATE_RECYCLE_BUFFER,					// 14

		//PROJECT_BANDPASS Object error codes:
		PROJECT_BANDPASS_FAILED_TO_ALLOCATE_HAMMING_WINDOW,						// 15
		PROJECT_BANDPASS_FAILED_TO_ALLOCATE_HANNING_WINDOW,						// 16

		//PROJECT_CARDIACATTRIBUTES Object error codes:
		PROJECT_CARD_ATTR_FAILED_TO_ALLOCATE_VECTOR_CARD_SCORES,					// 17
		PROJECT_CARD_ATTR_FAILED_TO_ALLOCATE_CROSSCORRELATION_OBJECT,				// 18

		//PROJECT_CARDIACTEMPLATE Object error codes:
		PROJECT_CARDIACTEMPLATE_FAILED_TO_ALLOCATE_LEFT_MARKER_VECTOR,				// 19
		PROJECT_CARDIACTEMPLATE_FAILED_TO_ALLOCATE_RIGHT_MARKER_VECTOR,			// 20
		PROJECT_CARDIACTEMPLATE_FAILED_TO_ALLOCATE_HAMMING_WINDOWED,				// 21

		//PROJECT_CONSOLIDATION Object error codes:
		PROJECT_CONSOLIDATION_FAILED_TO_ALLOCATE_VECT_IDX_SCORES,					// 22
		PROJECT_CONSOLIDATION_FAILED_TO_ALLOCATE_HISTORIC_BUFFER,					// 23
		PROJECT_CONSOLIDATION_FAILED_TO_ALLOCATE_PRED_BUFFER,						// 24

		//PROJECT_HEARTRATE_PREDICTION Object error codes:
		PROJECT_HR_PREDICTION_FAILED_TO_ALLOCATE_INPUT_BUFFER,						// 25
		PROJECT_HR_PREDICTION_FAILED_TO_ALLOCATE_HR_BUFFER,						// 26
		PROJECT_HR_PREDICTION_FAILED_TO_ALLOCATE_COEFF_BUFFER,						// 27
		PROJECT_HR_PREDICTION_FAILED_TO_ALLOCATE_POLY_OBJECT,						// 28

		//PROJECT_RESPIRATORYMETRICS Object error codes:
		PROJECT_RESPIRATORYMETRICS_FAILED_TO_ALLOCATE_RESP_SIGNAL_BUFFER,			// 29
		PROJECT_RESPIRATORYMETRICS_FAILED_TO_ALLOCATE_RESP_RATE_BUFFER,			// 30
		PROJECT_RESPIRATORYMETRICS_FAILED_TO_ALLOCATE_ZERO_IDX_BUFFER,				// 31
		PROJECT_RESPIRATORYMETRICS_FAILED_TO_ALLOCATE_AVE_FILTER_OBECT,			// 32

		//PROJECT_WINDOWING Object error codes:
		PROJECT_WINDOWING_FAILED_TO_ALLOCATE_WINDOW_BUFFER,						// 33

		//PROJECT_RESPRECONSTRUCT Object error codes:
		PROJECT_RESPRECONSTRUCT_FAILED_TO_ALLOCATE_YMATRIX,						// 34
		PROJECT_RESPRECONSTRUCT_FAILED_TO_ALLOCATE_XMATRIX,						// 35
		PROJECT_RESPRECONSTRUCT_FAILED_TO_ALLOCATE_SLOPEMATRIX,					// 36

		//PROJECT_MA2 Object error codes:
		PROJECT_MA2_FAILED_TO_ALLOCATE_RAW_GAIN_OBJECT,							// 37
		PROJECT_MA2_FAILED_TO_ALLOCATE_FILT_GAIN_OBJECT,							// 38
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_GAIN_OBJECT,							// 39
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_LPF_OBJECT,							// 40
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_HPF_OBJECT,							// 41
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESPIRATORY_LPF_OBJECT,						// 42
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESPIRATORY_HPF_OBJECT,						// 43
		PROJECT_MA2_FAILED_TO_ALLOCATE_SIGNAL_SELECT_OBJECT,						// 44
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_PEAK_DETECTOR,						// 45
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_TEMPLATE,							// 46
		PROJECT_MA2_FAILED_TO_ALLOCATE_TEMPLATE_BUFFER,							// 47
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_METRICS_OBJECT,						// 48
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESPIRATORY_METRICS_OBJECT,					// 49
		PROJECT_MA2_FAILED_TO_ALLOCATE_DYNAMIC_RANGE_OBJECT,						// 50
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_POSTLPF_OBJECT,						// 51
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_POSTHPF_OBJECT,						// 52
		PROJECT_MA2_FAILED_TO_ALLOCATE_PROCESS_MUTEX_LOCK,							// 53
		PROJECT_MA2_FAILED_TO_ALLOCATE_ADPWIENER_OBJECT,							// 54
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_POST_DF_OBJECT,						// 55
		PROJECT_MA2_FAILED_TO_ALLOCATE_WINDOWED_TEMP_BUFFER,						// 56
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_DISTANCE_BUFFER,					// 57
		PROJECT_MA2_FAILED_TO_ALLOCATE_RIGHT_AO_POINTS_VECTOR,						// 58
		PROJECT_MA2_FAILED_TO_ALLOCATE_LEFT_AO_POINTS_VECTOR,						// 59
		PROJECT_MA2_FAILED_TO_ALLOCATE_RECORDED_HR_VALUES_BUFFER,					// 60
		PROJECT_MA2_FAILED_TO_ALLOCATE_DYNAMIC_PEAK_OBJECT,						// 61
		PROJECT_MA2_FAILED_TO_ALLOCATE_CONDITION_VARIABLE,							// 62
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARDIAC_ATTRIBUTES_OBJECT,					// 63
		PROJECT_MA2_FAILED_TO_ALLOCATE_CONSOLIDATION_OBJECT,						// 64
		PROJECT_MA2_FAILED_TO_ALLOCATE_CARD_TEMP_INSERTION_OBJECT,					// 65
		PROJECT_MA2_FAILED_TO_ALLOCATE_HR_PREDICTION_OBJECT,						// 66
		PROJECT_MA2_FAILED_TO_ALLOCATE_STATISTICS_OBJECT,							// 67
		PROJECT_MA2_FAILED_TO_ALLOCATE_BANDPASS_FILTER_OBJECT,						// 68
		PROJECT_MA2_FAILED_TO_ALLOCATE_PREDICTION_THREAD_OBJECT,					// 69
		PROJECT_MA2_FAILED_TO_ALLOCATE_PRED_THREAD_ATOMIC_BOOL,					// 70
		PROJECT_MA2_FAILED_TO_ALLOCATE_SIGNAL_THREAD_ATOMIC_BOOL,					// 71
		PROJECT_MA2_FAILED_TO_ALLOCATE_LEARNING_ATOMIC_BOOL,						// 72
		PROJECT_MA2_FAILED_TO_ALLOCATE_RIGHT_AO_PT_STATS,							// 73
		PROJECT_MA2_FAILED_TO_ALLOCATE_LEFT_AO_PT_STATS,							// 74
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_BANDPASS_FILTER_OBJECT,				// 75
		PROJECT_MA2_FAILED_TO_ALLOCATE_POWER_SPECTRUM_OBJECT,						// 76
		PROJECT_MA2_FAILED_TO_ALLOCATE_SIGNAL_THREAD_OBJECT,						// 77
		PROJECT_MA2_FAILED_TO_ALLOCATE_ATOMIC_SAMPLE_COUNT,						// 78
		PROJECT_MA2_FAILED_TO_ALLOCATE_SGOLAY_FILTER_OBJECT,						// 79
		PROJECT_MA2_FAILED_TO_ALLOCAE_RESP_SGOLAY_FILTER_OBJECT,					// 80
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_PROC_ATOMIC_BOOL_OBJECT,				// 81
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_PROC_MUTEX_OBJECT,						// 82
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_PROC_CONDITION_OBJECT,					// 83
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_PEAKALL_OBJECT,						// 84
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_SEIVE_OBJECT,							// 85
		PROJECT_MA2_FAILED_TO_ALLOCATE_RESP_RECONSTRUCT_OBJECT,					// 86
		PROJECT_MA2_FAILED_TO_START_RESP_PROC_THREAD,								// 87


		NUMBER_OF_ERROR_CODES													// 88
	};
	
	//PROJECT_OBJECT ERROR MESSAGES:
	constexpr char PROJECT_OBJECT_MEMORY_BUFF_ERROR_MSG[] = "Could not allocate memory for memory buffer.";

	//PROJECT_MA2 ERROR MESSAGES:
	constexpr char PROJECT_MA2_PROCESS_MUTEX_ERROR_MSG[] = "Could not allocate memory for process mutex lock.";
	constexpr char PROJECT_MA2_RAW_GAIN_ERROR_MSG[] = "Could not allocate memory for raw gain object.";
	constexpr char PROJECT_MA2_FILT_GAIN_ERROR_MSG[] = "Could not allocate memory for filt gain object.";
	constexpr char PROJECT_MA2_RESP_GAIN_ERROR_MSG[] = "Could not allocate memory for respiratory gain object.";
	constexpr char PROJECT_MA2_CARD_LPF_ERROR_MSG[] = "Could not allocate memory for cardiac low pass filter object.";
	constexpr char PROJECT_MA2_CARD_HPF_ERROR_MSG[] = "Could not allocate memory for cardiac high pass filter object.";
	constexpr char PROJECT_MA2_RESP_LPF_ERROR_MSG[] = "Could not allocate memory for respiratory low pass filter object.";
	constexpr char PROJECT_MA2_SIGNAL_SELECT_ERROR_MSG[] = "Could not allocate memory for signal discriminator object.";
	constexpr char PROJECT_MA2_CARD_PEAK_DETECT_ERROR_MSG[] = "Could not allocate memory for cardiac peak detection object.";
	constexpr char PROJECT_MA2_RESP_HPF_ERROR_MSG[] = "Could not allocate memory for respiratory high pass filter object.";
	constexpr char PROJECT_MA2_CARD_TEMP_ERROR_MSG[] = "Could not allocate memory for cardiac template object.";
	constexpr char PROJECT_MA2_TEMPLATE_BUFFER_ERROR_MSG[] = "Could not allocate memory for template buffer.";
	constexpr char PROJECT_MA2_CARDIAC_METRICS_ERROR_MSG[] = "Could not allocate memory for cardiac metrics object.";
	constexpr char PROJECT_MA2_RESPIRATORY_METRICS_ERROR_MSG[] = "Could not allocate memory for respiratory metrics objects.";
	constexpr char PROJECT_MA2_DYNAMIC_RANGE_ERROR_MSG[] = "Could not allocate memory for dynamic range control objects.";
	constexpr char PROJECT_MA2_CARDIAC_POSTLPF_ERROR_MSG[] = "Could not allocate memory for cardiac post low pass filter objects.";
	constexpr char PROJECT_MA2_CARDIAC_POSTHPF_ERROR_MSG[] = "Could not allocate memory for cardiac post high pass filter objects.";
	constexpr char PROJECT_MA2_ADPWIENER_ERROR_MSG[] = "Could not allocate memory for adaptive wiener filter object.";
	constexpr char PROJECT_MA2_CARDIAC_POSTDF_ERROR_MSG[] = "Could not allocate memory for cardiac post derivative filter object";
	constexpr char PROJECT_MA2_WINDOWED_TEMP_BUFFER_ERROR_MSG[] = "Could not allocate memory for windowed cardiac template buffer.";
	constexpr char PROJECT_MA2_CARDIAC_DISTANCE_BUFFER_ERROR_MSG[] = "Could not allocate memory for cardiac distance buffer.";
	constexpr char PROJECT_MA2_CARDIAC_RIGHT_AO_POINTS_ERROR_MSG[] = "Could not allocate memory for right ao points vector.";
	constexpr char PROJECT_MA2_CARDIAC_LEFTT_AO_POINTS_ERROR_MSG[] = "Could not allocate memory for left ao points vector.";
	constexpr char PROJECT_MA2_HR_VALUES_BUFFER_ERROR_MSG[] = "Could not allocate memory for heart rate values buffer.";
	constexpr char PROJECT_MA2_DYNAMIC_PEAK_OBJECT_ERROR_MSG[] = "Could not allocate memory for dynamic peak object.";
	constexpr char PROJECT_MA2_CONDITION_VARIABLE_ERROR_MSG[] = "Could not allocate memory for condition variable.";
	constexpr char PROJECT_MA2_CARDIAC_ATTRIBUTES_OBJECT_ERROR_MSG[] = "Could not allocate memory for cadiac attributes object.";
	constexpr char PROJECT_MA2_CONDSOLIDATION_OBJECT_ERROR_MSG[] = "Could not allocate memory for consolidation object.";
	constexpr char PROJECT_MA2_CARD_TEMP_INSERTION_OBJECT_ERROR_MSG[] = "Could not allocate memory for cardiac template insertion object.";
	constexpr char PROJECT_MA2_HR_PREDICTION_OBJECT_ERROR_MSG[] = "Could not allocate memory for heart rate prediction object.";
	constexpr char PROJECT_MA2_STATISTICS_OBJECT_ERROR_MSG[] = "Could not allocate memory for statistics object.";
	constexpr char PROJECT_MA2_BANDPASS_FILTER_OBJECT_ERROR_MSG[] = "Could not allocate memory for bandpass filter object.";
	constexpr char PROJECT_MA2_PREDICTION_THREAD_OBJECT_ERROR_MSG[] = "Could not allocate memory for prediction thread object.";
	constexpr char PROJECT_MA2_PRED_THREAD_ATOMIC_BOOL_MSG[] = "Could not allocate memory for prediction thread atomic boprojectn.";
	constexpr char PROJECT_MA2_SIGNAL_THREAD_ATOMIC_BOOL_MSG[] = "Could not allocate memory for signal thread atomic boprojectn.";
	constexpr char PROJECT_MA2_LEARNING_ATOMIC_BOOL_MSG[] = "Could not allocate memory for learning atomic boprojectn.";
	constexpr char PROJECT_MA2_RIGHT_AO_PT_STATS_ERROR_MSG[] = "Could not allocate memory for right AO peak-trough stats container.";
	constexpr char PROJECT_MA2_LEFT_AO_PT_STATS_ERROR_MSG[] = "Could not allocate memory for left AO peak-trough stats container.";
	constexpr char PROJECT_MA2_RESP_BANDPASS_FILTER_OBJECT_ERROR_MSG[] = "Could not allocate memory for respiratory bandpass filter object.";
	constexpr char PROJECT_MA2_POWER_SPECTRUM_OBJECT_ERROR_MSG[] = "Could not allocate memory for power spectrum object.";
	constexpr char PROJECT_MA2_SIGNAL_THREAD_OBJECT_ERROR_MSG[] = "Could not allocate and start signal checking thread.";
	constexpr char PROJECT_MA2_ATOMIC_SAMPLE_COUNT_MSG[] = "Could not allocate atomic saample counter.";
	constexpr char PROJECT_MA2_RESP_SGOLAY_FILTER_ERROR_MSG[] = "Could not allocate memory for resp sgolay filter object.";
	constexpr char PROJECT_MA2_ATOMIC_BOOL_RESP_THREAD_ERROR_MSG[] = "Could not allocate memory for resp thread atomic boprojectn object.";
	constexpr char PROJECT_MA2_RESP_THREAD_MUTEX_ERROR_MSG[] = "Could not allocate memory for resp thread mutex lock object.";
	constexpr char PROJECT_MA2_RESP_THREAD_CONDITION_VARIABLE_ERROR_MSG[] = "Could not allocate condition variable for resp thread.";
	constexpr char PROJECT_MA2_RESP_PEAK_ALL_ERROR_MSG[] = "Could not allocate memory for resp peak all object.";
	constexpr char PROJECT_MA2_RESP_SEIVE_ERROR_MSG[] = "Could not allocate memory for resp seive object.";
	constexpr char PROJECT_MA2_RESP_RECONSTRUCTOR_ERROR_MSG[] = "Could not allocate memory for resp reconstruction object.";
	constexpr char PROJECT_MA2_RESP_THREAD_ERROR_MSG[] = "Could not start the respiratory processing thread.";
	constexpr char PROJECT_MA2_FINAL_SGOLAY_SMOOTHING_ERROR_MSG[] = "Could not allocate memory for Savitzky-Golay filter object.";


	//PROJECT_FILTER ERROR MESSAGES:
	constexpr char PROJECT_FILTER_ACOEFF_VECT_ERROR_MSG[] = "Could not allocate memory for A-coeffiecents vector object.";
	constexpr char PROJECT_FILTER_BCOEFF_VECT_ERROR_MSG[] = "Could not allocate memory for B-coeffiecents vector object.";
	constexpr char PROJECT_FILTER_ANALOGPOLE_VECT_ERROR_MSG[] = "Could not allocate memory for analog pole vector object.";
	constexpr char PROJECT_FILTER_DIGITALPOLE_VECT_ERROR_MSG[] = "Could not allocate memory for digital pole vector object.";
	constexpr char PROJECT_FILTER_ZEROVALUE_VECT_ERROR_MSG[] = "Could not allocate memory for zero value vector object.";
	constexpr char PROJECT_FILTER_MUTEXLOCK_ERROR_MSG[] = "Could not allocate memory for mutex lock object.";

	//PROJECT_CARDIACMETRICS ERROR MESSAGES:
	constexpr char PROJECT_CARDIACMETRICS_INDEX_VECTOR_ERROR_MSG[] = "Could not allocate memory for index location vector.";
	constexpr char PROJECT_CARDIACMETRICS_HR_CIRC_BUFFER_ERROR_MSG[] = "Could not allocate memory for hr circular buffer.";
	constexpr char PROJECT_CARDIACMETRICS_HRV_CIRC_BUFFER_ERROR_MSG[] = "Could not allocate memory for hrv circular buffer.";

	//PROJECT_CROSSCORRELATION ERROR MESSAGES:
	constexpr char PROJECT_CROSSCORRELATION_MAGNITUDE_BUFFER_ERROR_MSG[] = "Could not allocate memory for magnitude buffer.";
	constexpr char PROJECT_CROSSCORRELATION_RECYCLE_BUFFER_ERROR_MSG[] = "Could not allocate memory for recycle buffer.";

	//PROJECT_ADPWIENERFILTER ERROR MESSAGES:
	constexpr char PROJECT_ADPWIENERFILTER_RECYCLE_BUFFER_ERROR_MSG[] = "Could not allocate memory for recycle buffer.";

	//PROJECT_FFT ERROR MESSAGE:
	constexpr char PROJECT_FFT_TEMP_BUFFER_ERROR_MSG[] = "Could not allocate memory for temporary buffer.";

	//PROJECT_BANDPASS ERROR MESSAGES:
	constexpr char PROJECT_BANDPASS_HAMMING_WIND_ERROR_MSG[] = "Could not allocate memory for Hamming window object.";
	constexpr char PROJECT_BANDPASS_HANNING_WIND_ERROR_MSG[] = "Could not allocate memory for Hanning window object.";

	//PROJECT_CARDIACATTRIBUTES ERROR MESSAGES:
	constexpr char PROJECT_CARD_ATTR_VECTOR_CARD_SCORE_ERROR_MSG[] = "Could not allocate memory for vector of cardiac scores.";
	constexpr char PROJECT_CARD_ATTR_CROSSCORRELATION_ERROR_MSG[] = "Could not allocate memory for cross correlation objects.";

	//PROJECT_CARDIACTEMPLATE ERROR MESSAGES:
	constexpr char PROJECT_CARDIACTEMPLATE_LEFT_NODES_ERROR_MSG[] = "Could not allocate memory for cardiac left markers struct.";
	constexpr char PROJECT_CARDIACTEMPLATE_RIGHT_NODES_ERROR_MSG[] = "Could not allocate memory for cardiac right markers struct.";
	constexpr char PROJECT_CARDIACTEMPLATE_HAMMING_WIND_ERROR_MSG[] = "Could not allocate memory for hamming window.";

	//PROJECT_CONSOLIDATION ERROR MESSAGES:
	constexpr char PROJECT_CONSOLIDATION_VECT_IDX_SCORE_ERROR_MSG[] = "Could not allocate memory for vector index scores.";
	constexpr char PROJECT_CONSOLIDATION_HISTORIC_BUFFER_ERROR_MSG[] = "Could not allocate memory for historic peak location buffer.";
	constexpr char PROJECT_CONSOLIDATION_PRED_BUFFER_ERROR_MSG[] = "Could not allocate memory for pred peak location buffer.";

	//PROJECT_HEARTRATE_PREDICTION ERROR MESSAGES:
	constexpr char PROJECT_HR_PREDICTION_INPUT_BUFF_ERROR_MSG[] = "Could not allocate memory for input buffer.";
	constexpr char PROJECT_HR_PREDICTION_HR_BUFFER_ERROR_MSG[] = "Could not allocate memory for heart rate buffer.";
	constexpr char PROJECT_HR_PREDICTION_COEFF_BUFFER_ERROR_MSG[] = "Could not allocate memory for coefficient buffer.";
	constexpr char PROJECT_HR_PREDICTION_POLYNOMIAL_OBJ_ERROR_MSG[] = "Could not allocate memroy for polynomial object.";

	//PROJECT_RESPIRATORYMETRICS ERROR MESSAGES:
	constexpr char PROJECT_RESPIRATORYMETRICS_RESP_SIGNAL_BUFF_ERROR_MSG[] = "Could not allocate memory for respiratory signal buffer.";
	constexpr char PROJECT_RESPIRATORYMETRICS_RESP_RATE_BUFF_ERROR_MSG[] = "Could not allocate memory for respiratory rate buffer.";
	constexpr char PROJECT_RESPIRATORYMETRICS_ZERO_IDX_BUFF_ERROR_MSG[] = "Could not allocate memory for zero index buffer.";
	constexpr char PROJECT_RESPIRATORYMETRICS_AVERAGE_FILTER_ERROR_MSG[] = "Could not allocate memory for moving average filter object.";

	//PROJECT_WINDOWING ERROR MESSAGE:
	constexpr char PROJECT_WINDOWING_WINDOW_BUFFER_ERROR_MSG[] = "Could not allocate memory for windowed buffer.";

	//PROJECT RESPIRATORY RECONSTRUCTION ERROR MESSAGE:
	constexpr char PROJECT_RESP_RECONSTRUCTION_XMATRIX_ERROR_MSG[] = "Could not allocate memory for X value matrix object.";
	constexpr char PROJECT_RESP_RECONSTRUCTION_YMATRIX_ERROR_MSG[] = "Could not allocate memory for Y values matrix object.";
	constexpr char PROJECT_RESP_RECONSTRUCTION_SLOPE_MATRIX_ERROR_MSG[] = "Coult not allocate memory for slope matrix object.";
}
#endif